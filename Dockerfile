FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN export MAVEN_OPTS='-Xmx512m -XX:MaxPermSize=128m' && mvn -DskipTests=true package
RUN cd target && ls
EXPOSE 8082
ENTRYPOINT ["java","-jar","target/CustomerMicroService-0.0.1-SNAPSHOT.jar"]
